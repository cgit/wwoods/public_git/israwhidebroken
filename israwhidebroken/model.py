from datetime import datetime
import pkg_resources
pkg_resources.require("SQLObject>=0.10.1")
from turbogears.database import PackageHub
# import some basic SQLObject classes for declaring the data model
# (see http://www.sqlobject.org/SQLObject.html#declaring-the-class)
from sqlobject import SQLObject, SQLObjectNotFound, MultipleJoin, RelatedJoin
# import some datatypes for table columns from SQLObject
# (see http://www.sqlobject.org/SQLObject.html#column-types for more)
from sqlobject import StringCol, IntCol, DateTimeCol, ForeignKey
from turbogears import identity

__connection__ = hub = PackageHub('israwhidebroken')


# your data model

class Tree(SQLObject):
    arch = StringCol(length=10, notNone=True)
    # The compose ID links together a given day's rawhide trees, even if
    # everything else is missing - so if there's no PPC tree_time/repo_time
    # we can still have a ppc Tree for that day
    compose_id = IntCol(notNone=True)
    # These could be null if repodata/images are missing.
    # They're IntCol because I don't want to deal with converting
    # between the representation in the file(s) and weirdo datetime
    # or timestamp columns.
    tree_time = IntCol()
    repodata_time = IntCol()
    # A tree can have many results you might want to look at
    results = MultipleJoin('TestResult')

class Test(SQLObject):
    name = StringCol(notNone=True, unique=True)
    short_desc = StringCol(notNone=True)
    uri = StringCol(notNone=True)
    # Tests also have many results but we don't usually care about viewing
    # every single test result ever for a given test. Hence no join here.

class TestResult(SQLObject):
    # References to other objects - each test result comes from running one
    # Test on a given Tree.
    test = ForeignKey('Test')
    tree = ForeignKey('Tree')
    # result is just an int - 0 for fail, >= 1 for pass.
    # It's not a float 'cuz we don't have any performance tests in RATS.
    # XXX - How do we want to handle WARN or ERROR?
    result = IntCol(notNone=True)
    # userid of the user who submitted the result
    userid = IntCol(notNone=True)
    # bug_id for further information, esp. if the test is a fail.
    # XXX list of bug IDs?
    bug_id = IntCol()
    # timestamp this result was entered
    timestamp = DateTimeCol(notNone=True, default=datetime.now)
