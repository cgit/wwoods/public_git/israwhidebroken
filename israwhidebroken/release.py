# Release information about israwhidebroken

version = "0.1"

description = "A simple web dashboard for Fedora Rawhide autoqa results."
long_description = """A simple web dashboard to collect and displays the
results of Fedora's Rawhide Acceptance Test Plan. Accepts automated result
submission (authenticated against FAS, using JSON RPC) and allows manual
submission of results (for non-automated tests)."""
author = "Will Woods"
email = "wwoods@redhat.com"
copyright = "Copyright (c) 2009 Red Hat, Inc."

# if it's open source, you might want to specify these
# url = "http://yourcool.site/"
# download_url = "http://yourcool.site/download"
license = "GPLv2+" # XXX AGPL?
