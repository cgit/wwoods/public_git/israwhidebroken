#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Start script for the israwhidebroken TurboGears project.

This script is only needed during development for running from the project
directory. When the project is installed, easy_install will create a
proper start script.
"""

import sys
from israwhidebroken.commands import start, ConfigurationError

# jsonfas2
from turbogears import startup
from fedora.tg.util import enable_csrf
startup.call_on_startup.append(enable_csrf)

if __name__ == "__main__":
    try:
        start()
    except ConfigurationError, exc:
        sys.stderr.write(str(exc))
        sys.exit(1)
