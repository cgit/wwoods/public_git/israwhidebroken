# -*- coding: utf-8 -*-
"""Setup the israwhidebroken database with some initial values"""

import sys
from os import getcwd
from os.path import dirname, exists, join

# Enumerate the basic test cases defined in the test plan (see URL)
# https://fedoraproject.org/wiki/QA:Rawhide_Acceptance_Test_Plan#Test_Cases
rats_tests = [
('Repodata validity',
 'Checks that the yum metadata for a given repository is valid',
 'http://fedoraproject.org/wiki/QA:Repodata_validity_test_case'),

('comps.xml validity',
 'Verifies that comps.xml is usable by the installer and other system tools',
 'http://fedoraproject.org/wiki/QA:Comps_Validity_Test_Case'),

('Core package dependency closure',
 'Ensure that the Critical Path Packages have no conflicts or missing deps',
 'http://fedoraproject.org/wiki/QA:Core_package_dependency_closure_test_case'),

('Core package existence',
 'Ensure that the Critical Path Packages are present and not corrupted',
 'http://fedoraproject.org/wiki/QA:Core_package_existence_test_case'),

('Installer image existence',
 'Check that installer images and metadata are present and not corrupt',
 'http://fedoraproject.org/wiki/QA:Installer_image_presence_test_case'),

('Kernel boot',
 'Check that the installer kernel boots',
 'http://fedoraproject.org/wiki/QA:Kernel_simple_boot_test_case'),

('Anaconda loader fetching stage2',
 'Ensure that anaconda\'s first stage can fetch stage2 (install.img)',
 'http://fedoraproject.org/wiki/QA:Anaconda_stage2_fetch_test_case'),

('Anaconda stage2 disk probe',
 'Check that anaconda can detect the presence of disk devices',
 'http://fedoraproject.org/wiki/QA:Anaconda_storage_probe_test_case'),

('Anaconda package installation',
 'Check that anaconda is able to install packages to disk',
 'http://fedoraproject.org/wiki/QA:Anaconda_package_install_test_case'),

('Anaconda bootloader setup',
 'Check that anaconda can set up the bootloader',
 'http://fedoraproject.org/wiki/QA:Anaconda_bootloader_setup_test_case'),

('X startup/basic display configuration',
 'Check that Xorg can detect and configure the video controller and monitor',
 'http://fedoraproject.org/wiki/QA:X_basic_display_test_case'),

('X basic input handling',
 'Ensure that the X server can receive and process input',
 'http://fedoraproject.org/wiki/QA:X_basic_input_handling_test_case'),

('Basic network connectivity',
 'A very simple networking test',
 'http://fedoraproject.org/wiki/QA:Network_basic_test_case'),

('yum update functionality',
 'A very simple check of \'yum update\' functionality',
 'http://fedoraproject.org/wiki/QA:Yum_simple_update_test_case'),
]

treedata = [
# These 3 items correspond to the Fedora 11 trees
{'arch':'i386',
 'compose_id':2009060201, 'tree_time':1243980101, 'repodata_time':1243979414},
{'arch':'x86_64',
 'compose_id':2009060201, 'tree_time':1243980953, 'repodata_time':1243980178},
{'arch':'ppc',
 'compose_id':2009060201, 'tree_time':1243981037, 'repodata_time':1243979681},
]

def setup_database():
    # Load the models
    from israwhidebroken import model

    # Add tests to the test table
    # XXX force test IDs?
    for (n, sd, u) in rats_tests:
        exists = model.Test.select(model.Test.q.name==n)
        if exists.count() == 0:
            print "adding Test(%s)" % n
            test = model.Test(name=n, short_desc=sd, uri=u)
        else:
            print "Test(%s) already exists" % n
    # Add example trees
    for t in treedata:
        exists = model.Tree.selectBy(arch=t['arch'],compose_id=t['compose_id'])
        if exists.count() == 0:
            print "adding Tree(%u-%s)" % (t['compose_id'], t['arch'])
            tree = model.Tree(**t)
        else:
            print "Tree(%u-%s) already exists" % (t['compose_id'], t['arch'])

    model.hub.commit()

    # TODO: add a couple of example results

    print "Successfully setup"

if __name__ == '__main__':
    import turbogears
    setupdir = dirname(__file__)
    curdir = getcwd()
    if len(sys.argv) > 1:
        configfile = sys.argv[1]
    elif exists(join(setupdir, "setup.py")):
        configfile = join(setupdir, "dev.cfg")
    elif exists(join(curdir, "prod.cfg")):
        configfile = join(curdir, "prod.cfg")
    print "using config %s" % configfile
    turbogears.update_config(configfile=configfile,
                             modulename="israwhidebroken.config")
    setup_database()
