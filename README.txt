israwhidebroken

This is a TurboGears (http://www.turbogears.org) project. It can be
started by running the start-israwhidebroken.py script.

Use the populate-db.py script to add some basic data to the database - the
test list (required) and a set of 3 Trees (which can be deleted once you have
added newer data).
